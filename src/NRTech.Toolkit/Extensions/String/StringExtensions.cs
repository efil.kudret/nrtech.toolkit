﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Utf8Json;
using Utf8Json.Resolvers;

namespace NRTech.Toolkit
{
    public static class StringExtensions
    {
        public static T FromJson<T>(this string data)
        {
            return JsonSerializer.Deserialize<T>(data, StandardResolver.ExcludeNullCamelCase);
        }

        public static T FromJson<T>(this string data, IJsonFormatterResolver resolver)
        {
            if (resolver is null)
                return data.FromJson<T>();
            return JsonSerializer.Deserialize<T>(data, resolver);
        }

        public static string ToJson(this object o)
        {
            return JsonSerializer.ToJsonString(o, StandardResolver.ExcludeNullCamelCase);
        }

        public static string ToJson(this object o, IJsonFormatterResolver resolver)
        {
            if (resolver is null)
                return o.ToJson();
            return JsonSerializer.ToJsonString(o, resolver);
        }

        public static string GetJsonHash(this string json, string secret, bool isBase64 = false)
        {
            secret ??= "";
            var encoding = new ASCIIEncoding();
            var keyByte = encoding.GetBytes(secret);
            var messageBytes = encoding.GetBytes(json);
            using var hcr = new HMACSHA256(keyByte);
            var hash = hcr.ComputeHash(messageBytes);
            return isBase64 ? Convert.ToBase64String(hash) : ByteToString(hash).ToLowerInvariant();
        }

        public static string GetSHA256Hash(this string input)
        {
            var sha256 = System.Security.Cryptography.SHA256.Create();
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            var hashBytes = sha256.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (var t in hashBytes)
                sb.Append(t.ToString("X2"));
            return sb.ToString();
        }

        static string ByteToString(byte[] buff)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < buff.Length; i++)
                sb.Append(buff[i].ToString("X2"));
            return sb.ToString();
        }

        public static string ToUnderscoreCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";
            return string.Concat(str.Select((x, i) =>
                i > 0 && char.IsUpper(x) ? "_" + x.ToString().ToLowerInvariant() : x.ToString().ToLowerInvariant()));
        }

        public static string ToCamelCase(this string str)
        {
            return string.Concat(str.Select((x, i) => i == 0 ? x.ToString().ToLowerInvariant() : x.ToString()));
        }

        public static string ToKebabCase(this string source, string splitter, bool uppercase)
        {
            if (source is null) return null;

            if (source.Length == 0) return string.Empty;

            var builder = new StringBuilder();
            for (var i = 0; i < source.Length; i++)
            {
                if (char.IsLower(source[i])) // if current char is already lowercase
                {
                    builder.Append(source[i]);
                }
                else if (i == 0) // if current char is the first char
                {
                    builder.Append(char.ToLower(source[i]));
                }
                else if (char.IsLower(source[i - 1])) // if current char is upper and previous char is lower
                {
                    builder.Append(splitter);
                    builder.Append(char.ToLower(source[i]));
                }
                else if (i + 1 == source.Length || char.IsUpper(source[i + 1])
                ) // if current char is upper and next char doesn't exist or is upper
                {
                    builder.Append(char.ToLower(source[i]));
                }
                else // if current char is upper and next char is lower
                {
                    builder.Append(splitter);
                    builder.Append(char.ToLower(source[i]));
                }
            }

            return uppercase ? builder.ToString().ToUpperInvariant() : builder.ToString();
        }
        
        public static IEnumerable<byte> GetBytes(this string data)
        {
            var numberChars = data.Length;
            var bytes = new byte[numberChars / 2];
            for (var i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(data.Substring(i, 2), 16);
            return bytes;
        }
    }
}