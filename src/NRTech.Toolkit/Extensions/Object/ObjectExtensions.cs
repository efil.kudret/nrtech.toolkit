﻿namespace NRTech.Toolkit
{
    using System;

    public static class ObjectExtensions
    {
        public static bool IsNull(this object o)
        {
            try
            {
                if (object.ReferenceEquals(o, null))
                    return true;
                return false;
            }
            catch
            {
                return true;
            }
        }

        public static bool IsNotNull(this object o)
        {
            return !o.IsNull();
        }
        
        public static bool IsDefault(this object o)
        {
            try
            {
                if (o.IsNull())
                    return true;
                
                string typeString = o.GetType().Name;
                switch (typeString)
                {
                    case "String":
                        return string.IsNullOrWhiteSpace(o.ToString());
                    case "Int32":
                        return (int)o == 0;
                    case "Int64":
                        return (long)o == 0;
                    case "Decimal":
                        return (decimal)o == 0m;
                    case "Double":
                        return (double)o == 0d;
                    case "Single":
                        return (float)o == 0f;
                    case "DateTime":
                        return ((DateTime)o) == DateTime.MinValue;
                    case "DBNull":
                        return true;
                    default:
                        return o == null;
                }

            }
            catch
            {
                return true;
            }
        }

        public static bool IsNotDefault(this object o)
        {
            return !o.IsDefault();
        }
    }
}
