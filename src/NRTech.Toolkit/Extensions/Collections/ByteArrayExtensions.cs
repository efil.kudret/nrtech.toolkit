using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NRTech.Toolkit.Extensions
{
    public static class ByteArrayExtensions
    {
        public static string GetHex(this IEnumerable<byte> data)
        {
            var hex = new StringBuilder(data.Count() * 2);
            foreach (var b in data)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
    }
}