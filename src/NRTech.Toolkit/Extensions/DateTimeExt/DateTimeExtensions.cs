using System;

namespace NRTech.Toolkit
{
    public static class DateTimeExtensions
    {
        public static System.DateTime GetDateTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static int GetTimeEpoch(this DateTime dateTime)
        {
            var epoch = (int) (dateTime - new DateTime(1970, 1, 1)).TotalSeconds;
            return epoch;
        }
    }
}