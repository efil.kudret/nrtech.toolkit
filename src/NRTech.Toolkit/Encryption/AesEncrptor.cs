namespace NRTech.Toolkit.Encryption
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Security.Cryptography;
    using System.Text;
    using NRTech.Toolkit.Extensions;

    public class AesEncryptor
    {
        #region Encrypt

        public static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            using (MemoryStream ms = new MemoryStream())
            using (Aes alg = Aes.Create())
            {
                alg.Key = Key;
                alg.IV = IV;
                using (CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearData, 0, clearData.Length);
                    cs.Close();
                    byte[] encryptedData = ms.ToArray();
                    return encryptedData;
                }
            }
        }

        public static string Encrypt(string clearText, string Password)
        {
            byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(clearText);
            using (PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 }))
            {
                byte[] encryptedData = Encrypt(clearBytes, pdb.GetBytes(32), pdb.GetBytes(16));
                return Convert.ToBase64String(encryptedData);
            }
        }

        public static byte[] Encrypt(byte[] clearData, string Password)
        {
            using (PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 }))
                return Encrypt(clearData, pdb.GetBytes(32), pdb.GetBytes(16));
        }

        public static void Encrypt(string fileIn, string fileOut, string Password)
        {
            using (FileStream fsIn = new FileStream(fileIn, FileMode.Open, FileAccess.Read))
            using (FileStream fsOut = new FileStream(fileOut, FileMode.OpenOrCreate, FileAccess.Write))
            using (PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 }))
            using (Aes alg = Aes.Create())
            {
                alg.Key = pdb.GetBytes(32);
                alg.IV = pdb.GetBytes(16);
                using (CryptoStream cs = new CryptoStream(fsOut, alg.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int bytesRead;
                    do
                    {
                        bytesRead = fsIn.Read(buffer, 0, bufferLen);
                        cs.Write(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                }
            }
        }

        public static void Encrypt(object obj, string fileOut, string password)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream fs = new FileStream(fileOut, FileMode.OpenOrCreate))
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                var data = Encrypt(ms.GetBuffer(), password);
                fs.Write(data, 0, data.Length);
            }
        }

        public static string Encrypt(object obj, string password)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                var data = Encrypt(ms.GetBuffer(), password);
                return data.GetHex();
            }
        }

        #endregion

        #region Decrypt

        public static byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
        {
            using (MemoryStream ms = new MemoryStream())
            using (Aes alg = Aes.Create())
            {
                alg.Key = Key;
                alg.IV = IV;
                using (CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherData, 0, cipherData.Length);
                    cs.Close();
                    byte[] decryptedData = ms.ToArray();
                    return decryptedData;
                }
            }
        }

        public static string Decrypt(string cipherText, string Password)
        {
            using (PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 }))
            {
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                byte[] decryptedData = Decrypt(cipherBytes, pdb.GetBytes(32), pdb.GetBytes(16));
                return System.Text.Encoding.Unicode.GetString(decryptedData);
            }
        }

        public static byte[] Decrypt(byte[] cipherData, string Password)
        {
            using (PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 }))
                return Decrypt(cipherData, pdb.GetBytes(32), pdb.GetBytes(16));
        }

        public static void Decrypt(string fileIn, string fileOut, string Password)
        {
            using (FileStream fsIn = new FileStream(fileIn, FileMode.Open, FileAccess.Read))
            using (FileStream fsOut = new FileStream(fileOut, FileMode.OpenOrCreate, FileAccess.Write))
            using (PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 }))
            using (Aes alg = Aes.Create())
            {
                alg.Key = pdb.GetBytes(32);
                alg.IV = pdb.GetBytes(16);
                using (CryptoStream cs = new CryptoStream(fsOut, alg.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int bytesRead;
                    do
                    {
                        bytesRead = fsIn.Read(buffer, 0, bufferLen);
                        cs.Write(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                }
            }
        }

        public static T Decrypt<T>(string file, string password)
        {
            using (FileStream fs = new FileStream(file, FileMode.Open))
            using (Aes alg = Aes.Create())
            {
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                BinaryFormatter bf = new BinaryFormatter();
                using (MemoryStream msParsed = new MemoryStream(Decrypt(buffer, password)))
                {
                    var obj = bf.Deserialize(msParsed);
                    if (obj is T)
                        return (T)obj;
                    else
                        throw new System.Exception("Unknown object type.");
                }
            }
        }

        public static T Decrypt<T>(string data, string password, bool isStringData)
        {
            using (var alg = Aes.Create())
            {
                var buffer = data.GetBytes().ToArray();
                var de = Decrypt(buffer, password);

                var bf = new BinaryFormatter();
                using (var msParsed = new MemoryStream(de))
                {
                    var obj = bf.Deserialize(msParsed);
                    if (obj is T)
                        return (T)obj;
                    else
                        throw new System.Exception("Unknown object type.");
                }
            }
        }

        #endregion   
    }
}