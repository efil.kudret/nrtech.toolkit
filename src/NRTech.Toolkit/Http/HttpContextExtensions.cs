using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace NRTech.Toolkit
{
    public static class HttpContextExtensions
    {
        public static async Task<string> GetRequestBody(this HttpContext context)
        {
            using var reader = new StreamReader(context.Request.Body, Encoding.UTF8);
            var body = await reader.ReadToEndAsync();
            body = body.Replace("\n", "");
            body = body.Replace("\r", "");
            body = body.Replace("\t", "");
            return body;
        }
    }
}