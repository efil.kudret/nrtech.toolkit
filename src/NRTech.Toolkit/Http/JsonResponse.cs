﻿using System.Net;

namespace NRTech.Toolkit.Http
{
    public class JsonResponse 
    {
        public HttpStatusCode Status { get; set; }
        public string Error { get; set; }
        public string Content { get; set; }
    }
    
    public class JsonResponse<T> : JsonResponse
    {
        public T Result { get; set; }
    }
}