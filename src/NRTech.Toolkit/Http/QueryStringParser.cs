using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace NRTech.Toolkit
{
    public enum PropertyNameTypoTypes
    {
        CamelCase,
        PascalCase,
        SnakeCase
    }
    
    public static class QueryStringParser
    {
        private static string GetFromTypo(string name, PropertyNameTypoTypes typo)
        {
            switch (typo)
            {
                case PropertyNameTypoTypes.CamelCase:
                    return name.ToCamelCase();
                case PropertyNameTypoTypes.PascalCase:
                    return name;
                case PropertyNameTypoTypes.SnakeCase:
                    return name;
                default:
                    return name;
            }
        }
        
        public static T GetFromQueryString<T>(this HttpContext httpContext, PropertyNameTypoTypes typo = PropertyNameTypoTypes.PascalCase, CultureInfo cultureInfo = null) where T : new()
        {
            if (cultureInfo.IsNull())
                cultureInfo = CultureInfo.InvariantCulture;
            
            var obj = new T();
            var properties = typeof(T).GetProperties();  
            foreach (var property in properties)
            {
                var getName = GetFromTypo(property.Name, typo);
                var valueAsString = httpContext.Request.Query[property.Name];
                var value = Parse(property.PropertyType, valueAsString, cultureInfo);
                if (value == null)
                    continue;
                property.SetValue(obj, value, null);  
            }
            return obj;
        }

        public static string ToQueryString(this object obj,
            PropertyNameTypoTypes typo = PropertyNameTypoTypes.CamelCase)
        {
            var buffer = new List<string>();
            var objType = obj.GetType();

            var properties = objType.GetProperties()
                .Where(p => p.GetValue(obj, null) != null);

            foreach (var prop in properties)
                buffer.Add(string.Join("=", GetFromTypo(prop.Name, typo), prop.GetValue(obj)));
            return string.Join("&", buffer);
        }
        
        private static object Parse(Type dataType, string valueToConvert, CultureInfo culture)
        {
            var obj = TypeDescriptor.GetConverter(dataType);
            var value = obj.ConvertFromString(null, culture, valueToConvert);
            return value;
        }
    }
}