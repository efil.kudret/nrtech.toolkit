﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Utf8Json;
using Utf8Json.Resolvers;

namespace NRTech.Toolkit.Http
{
    public class BasicAuthInfo
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class HttpClientOptions
    {
        public int Timeout { get; set; }
    }

    public class HttpJsonClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<HttpJsonClient> _logger;

        private HttpClient httpClient => _httpClientFactory.CreateClient();

        private HttpClientOptions _options;
        
        public HttpJsonClient(ILogger<HttpJsonClient> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        public void SetDefaultOptions()
        {
            _options = new HttpClientOptions
            {
                Timeout = 1000 * 60 * 5
            };
        }

        public async Task<T> GetDataAsync<T>(string url)
        {
            var response = await httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();
            return content.FromJson<T>();
        }

        public async Task<T> GetDataAsync<T>(string url, IJsonFormatterResolver resolver, HttpClientOptions options = null)
        {
            _options = options;
            if(_options.IsNull())
                SetDefaultOptions();
            using (var cts = new CancellationTokenSource(_options.Timeout))
            {
                var response = await httpClient.GetAsync(url);
                var content = await response.Content.ReadAsStringAsync();
                return content.FromJson<T>(resolver);   
            }
        }

        
        public async Task<JsonResponse<T>> GetDataAsync<T>(string url, object queryStringData, string bearerToken = null,
            IJsonFormatterResolver resolver = null, HttpClientOptions options = null)
        {
            _options = options;
            var qData = queryStringData.ToQueryString();
            if (!string.IsNullOrWhiteSpace(qData))
                qData = $"?{qData}";
            
            var request = new HttpRequestMessage(HttpMethod.Get, $"{url}{qData}");
            request.Headers.Clear();

            if (bearerToken.IsNotNull())
            {
                request.Headers.Authorization =
                    new AuthenticationHeaderValue("Bearer", bearerToken);
            }

            if (_options.IsNull())
                SetDefaultOptions();

            using (var cts = new CancellationTokenSource(_options.Timeout))
            {
                var response = await httpClient.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();
            
                var jsonResponse = new JsonResponse<T>()
                {
                    Status = response.StatusCode,
                    Content = content,
                    Result =content.FromJson<T>(resolver) 
                };
                return jsonResponse;   
            }
        }

        public async Task<string> GetDataAsync(string url)
        {
            if(_options.IsNull())
                SetDefaultOptions();
            using (var cts = new CancellationTokenSource(_options.Timeout))
            {
                var response = await httpClient.GetAsync(url);
                return await response.Content.ReadAsStringAsync();   
            }
        }
        
        public Task<string> GetDataAsync(string url, HttpClientOptions options)
        {
            _options = options;
            return GetDataAsync<string>(url);
        }

        public async Task<JsonResponse<T>> PostDataAsync<T>(string url, object data,
            IJsonFormatterResolver resolver = null, Dictionary<string, string> headers = null,
            BasicAuthInfo basicAuthInfo = null, HttpClientOptions options = null)
        {
            _options = options;
            var request = new HttpRequestMessage(HttpMethod.Post, url);

            var json = data.ToJson(resolver);
            var content = new StringContent(json);

            content.Headers.Clear();
            content.Headers.Add("Content-Type", "application/json");

            request.Content = content;
            request.Headers.Add("Accept", "*/*");

            if (!(headers is null))
                foreach (var val in headers)
                    request.Headers.Add(val.Key, val.Value);

            if (basicAuthInfo.IsNotNull())
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(
                        Encoding.UTF8.GetBytes($"{basicAuthInfo.Username}:{basicAuthInfo.Password}")));
            }

            if(_options.IsNull())
                SetDefaultOptions();
            
            using (var cts = new CancellationTokenSource(_options.Timeout))
            {
                var response = await httpClient.SendAsync(request);
                var result = await response.Content.ReadAsStringAsync();

                var jsonResponse = new JsonResponse<T>()
                {
                    Status = response.StatusCode,
                    Content = result
                };
                try
                {
                    if (result.IsNotDefault())
                        jsonResponse.Result = result.FromJson<T>(resolver);
                }
                catch (Exception e)
                {
                    jsonResponse.Error = e.Message;
                }

                return jsonResponse;
            }
        }

        public async Task<JsonResponse> PostDataAsync(string url, object data,
            IJsonFormatterResolver resolver = null, Dictionary<string, string> headers = null, HttpClientOptions options = null)
        {
            _options = options;
            using (var cts = new CancellationTokenSource(_options.Timeout))
            {
                var jsonResponse = new JsonResponse();
                try
                {
                    var message = new HttpRequestMessage(HttpMethod.Post, url);
                    var json = data.ToJson(resolver);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    message.Headers.Add("Accept", "application/json");
                    message.Content = content;

                    if (!(headers is null))
                        foreach (var val in headers)
                            message.Headers.Add(val.Key, val.Value);

                    var response = await httpClient.SendAsync(message);
                    var result = await response.Content.ReadAsStringAsync();

                    jsonResponse = new JsonResponse()
                    {
                        Status = response.StatusCode,
                        Content = result
                    };
                }
                catch (Exception e)
                {
                    jsonResponse.Error = e.Message;
                }

                return jsonResponse;
            }
        }
    }
}