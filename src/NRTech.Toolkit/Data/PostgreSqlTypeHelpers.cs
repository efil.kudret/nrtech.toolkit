﻿namespace NRTech.Toolkit.Data
{
    using System.Collections.Generic;
    
    public static class PostgreSqlTypeHelpers
    {
        public static string ToRawData(this System.Dynamic.ExpandoObject obj)
        {
            List<string> values = new List<string>();
            foreach (var item in obj as ICollection<KeyValuePair<string, object>>)
            {
                if (item.Value is null)
                    values.Add("");
                else
                    values.Add(item.Value.ToString());
            }
            return string.Format("({0})", string.Join(',', values));
        }
    }
}
